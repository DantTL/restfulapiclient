package com.restapicliet.respapicliet.service;

import com.restapicliet.respapicliet.ui.modelos.request.UserDetailsRequestModel;
import com.restapicliet.respapicliet.ui.modelos.response.UserRest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    List<UserRest> getUsers(int page, int limit);
    UserRest guardarUsuario(UserDetailsRequestModel userDetails);
    UserRest actualizarUsuario(UserRest userRest);
    UserRest getUserByUserId(String userId);
    String eliminarUsuario(String userId);
}
