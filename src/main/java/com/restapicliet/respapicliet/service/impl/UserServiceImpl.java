package com.restapicliet.respapicliet.service.impl;

import com.restapicliet.respapicliet.service.UserService;
import com.restapicliet.respapicliet.share.UserDto;
import com.restapicliet.respapicliet.ui.modelos.request.UserDetailsRequestModel;
import com.restapicliet.respapicliet.ui.modelos.response.UserRest;
import org.springframework.beans.BeanUtils;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public List<UserRest> getUsers(int page, int limit) {
        List<UserRest> returnValue = new ArrayList<>();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String resourseUrl = "http://localhost:8081/users?page=1";
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<UserRest[]> response = restTemplate.exchange(resourseUrl, HttpMethod.GET,entity, UserRest[].class);
        returnValue.addAll(Arrays.asList(response.getBody()));

        return returnValue;
    }

    @Override
    public UserRest guardarUsuario(UserDetailsRequestModel userDetails) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String resourseUrl = "http://localhost:8081/users";

        HttpEntity<UserDetailsRequestModel> requestEntity = new HttpEntity<>(userDetails,headers);

        ResponseEntity<UserRest> responseEntity = restTemplate.postForEntity(resourseUrl, requestEntity, UserRest.class);

        return responseEntity.getBody();
    }

    @Override
    public UserRest actualizarUsuario(UserRest userRest) {
        UserDetailsRequestModel userDetails = new  UserDetailsRequestModel();
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(userRest, userDto);
        BeanUtils.copyProperties(userDto, userDetails);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String resourseUrl = "http://localhost:8081/users/" + userRest.getUserId();

        HttpEntity<UserDetailsRequestModel> requestEntity = new HttpEntity<>(userDetails,headers);

        ResponseEntity<UserRest> responseEntity = restTemplate.exchange(resourseUrl,HttpMethod.PUT,requestEntity, UserRest.class);

        return responseEntity.getBody();
    }

    @Override
    public UserRest getUserByUserId(String userId) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        String resourseUrl = "http://localhost:8081/users/" + userId;
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<UserRest> response = restTemplate.exchange(resourseUrl,HttpMethod.GET,entity, UserRest.class);
        response.getBody();
        return response.getBody();
    }

    @Override
    public String eliminarUsuario(String userId) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        String resourseUrl = "http://localhost:8081/users/" + userId;
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<String> response = restTemplate.exchange(resourseUrl,HttpMethod.DELETE,entity, String.class);
        response.getBody();
        return response.getBody();
    }
}
